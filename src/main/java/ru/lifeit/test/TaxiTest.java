package ru.lifeit.test;

// https://code.google.com/archive/p/parse-cmd/
import ca.zmatrix.cli.ParseCmd;

import java.io.File;

import java.util.Map;

import java.util.concurrent.Executors;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.TimeUnit;

import java.util.concurrent.atomic.AtomicInteger;

public class TaxiTest extends Thread
{
	public static void main(String[] args)
	{
		ParseCmd pc = new ParseCmd.Builder()
			.help(USAGE)
			.parm(PARAM_IN, "100").rex("^[0-9]+$")
			.parm(PARAM_OUT, "10").rex("^[0-9]+$")
			.parm(PARAM_MAX, "1000").rex("^[0-9]+$")
			.build();
		String parseError = pc.validate(args);
		if(!pc.isValid(args))
		{
			System.out.println(parseError);
			return;
		}
		Map<String, String> params = pc.parse(args);
		try
		{
			clientThreadCount = Integer.parseInt(params.get(PARAM_IN));
			driverThreadCount = Integer.parseInt(params.get(PARAM_OUT));
			maxMessageCount = Integer.parseInt(params.get(PARAM_MAX));
		}
		catch(NumberFormatException e)
		{
			e.printStackTrace();
			System.err.println(parseError);
			return;
		}
		
		File dir = new File("data");
		if(dir.exists() && !dir.isDirectory())
		{
			System.err.println("Invalid directory name: "+dir.getAbsolutePath());
			return;
		}
		if(!dir.exists() && !dir.mkdirs())
		{
			System.err.println("Can\'t create directory: "+dir.getAbsolutePath());
			return;
		}
		for(int i = 1; i <= driverThreadCount; ++i) new File(dir, Integer.toString(i)).mkdir();
		
		TaxiTest dispatcher = new TaxiTest();
		dispatcher.start();
		try
		{
			dispatcher.join();
		}
		catch(InterruptedException e)
		{
			System.err.println("Interrupted");
			System.exit(1);
			return;
		}
	}
	
	private TaxiTest()
	{
		System.out.printf("Client threads: %d\nDriver threads: %d\nMax message count: %d\n", clientThreadCount, driverThreadCount, maxMessageCount);
		Runtime.getRuntime().addShutdownHook(new Thread()
		{
			@Override
			public void run()
			{
				running = false;
				clientWorkers.shutdownNow();
				try
				{
					TaxiTest.this.join();
				}
				catch(InterruptedException ignore) {}
				double elapsed = System.nanoTime() - startTime;
				System.out.printf("Bye, bye! %3.3f sec\n", elapsed / 1000000000d);
			}
		});
		
		clientWorkers = Executors.newFixedThreadPool(clientThreadCount);
		driverWorkers = new FixedSizeExecutorService(driverThreadCount);
	}
	
	@Override
	public void run()
	{
	    System.out.println("Start thread TaxiTest: "+Thread.currentThread());
		while(running)
		{
			final int nextId = nextIdGenerator.getAndIncrement();
			if(nextId > maxMessageCount) break;
			try
			{
				clientWorkers.execute(() ->
				{
					final Message msg;
					try
					{
						msg = Message.generate(nextId, driverThreadCount);
					}
					catch(Exception e1)
					{
						System.err.println("Message generation error! Id: "+nextId+". "+e1.getMessage());
						return;
					}
					//System.out.println("Message: "+msg);
					// save
					driverWorkers.execute(msg);
				});
			}
			catch(RejectedExecutionException e)
			{
			    System.out.println("Rejected. "+e.getMessage());
				Thread.yield();
			}
		}
		System.out.println("clients:");
		System.out.println(clientWorkers);
		System.out.println("drivers:");
		System.out.println(driverWorkers);
		
		shutdownClients();
		shutdownDrivers();
	}
	
	private void shutdownClients()
	{
		clientWorkers.shutdown();
		try
		{
			if(!clientWorkers.awaitTermination(1, TimeUnit.SECONDS))
			{
				clientWorkers.shutdownNow();
				if(!clientWorkers.awaitTermination(1, TimeUnit.SECONDS))
				{
					System.err.println("Pool did not terminate!");
					System.exit(1);
					return;
				}
			}
			else System.out.println("clientWorkers.awaitTermination OK");
		}
		catch(InterruptedException e)
		{
		    System.err.println("client workers awaitTermination interrupted");
			clientWorkers.shutdownNow();
			Thread.currentThread().interrupt();
		}
	}
	
	private void shutdownDrivers()
	{
		System.out.println("waiting 5 sec for drivers termination");
		driverWorkers.shutdown();
		try
		{
		    driverWorkers.awaitTermination(5, TimeUnit.SECONDS);
		}
		catch(InterruptedException e)
		{
		    e.printStackTrace();
		}
	}
	
	public static final String PARAM_IN = "-in";
	public static final String PARAM_OUT = "-out";
	public static final String PARAM_MAX = "-max";
	public static final String USAGE = "Usage: "+PARAM_IN+" N "+PARAM_OUT+" K "+PARAM_MAX+" max_message_count";
	
	private static int clientThreadCount;
	private static int driverThreadCount;
	private static int maxMessageCount;
	
	private volatile boolean running = true;
	private final long startTime = System.nanoTime();
	private final ExecutorService clientWorkers;
	private final ExecutorService driverWorkers;
	private final AtomicInteger nextIdGenerator = new AtomicInteger(1);
}
