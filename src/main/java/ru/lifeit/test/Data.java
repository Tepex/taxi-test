package ru.lifeit.test;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

class Data
{
	public Data(String value)
	{
		this.value = value;
	}
	
	public String getValue()
	{
		return value;
	}
	
	@Override
	public String toString()
	{
		return value;
	}
	
	public void append(Document doc, Element parent)
	{
		Element data = doc.createElement("data");
		data.appendChild(doc.createTextNode(value));
		parent.appendChild(data);
	}
	
	private String value;
}
