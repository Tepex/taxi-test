package ru.lifeit.test;

import java.security.AccessControlContext;
import java.security.AccessController;
import java.security.PrivilegedAction;

import java.util.ArrayList;
import java.util.List;

import java.util.concurrent.AbstractExecutorService;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;

import java.util.concurrent.atomic.AtomicInteger;

import java.util.concurrent.locks.AbstractQueuedSynchronizer;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

class FixedSizeExecutorService extends AbstractExecutorService
{
	public FixedSizeExecutorService(int poolSize)
	{
		this.poolSize = poolSize;
		acc = System.getSecurityManager() == null ? null : AccessController.getContext();
		workers = new Worker[poolSize];
	}
	
	@Override
	public void execute(Runnable cmd)
	{
		if(cmd == null) throw new NullPointerException("Task is null!");
		if(!(cmd instanceof Message)) throw new IllegalArgumentException("cmd must be instanceof Message!");
		Message msg = (Message)cmd;
		int workerIndex = msg.getTargetId()-1;
		if(workerIndex < 0 || workerIndex >= poolSize) throw new IllegalArgumentException("Invalid target ID! Must be > 0 and <= "+poolSize);
		
		if(state.get() >= SHUTDOWN) return;
		
        boolean newWorker = false;
		Worker worker = workers[workerIndex];
		if(worker == null)
		{
			final ReentrantLock mainLock = this.mainLock;
			mainLock.lock();
			try
			{
				worker = workers[workerIndex];
				if(worker == null && state.get() < SHUTDOWN)
				{
					workers[workerIndex] = worker = new Worker(msg.getTargetId());
					newWorker = true;
				}
			}
			finally
			{
				mainLock.unlock();
			}
		}
		
		if(!worker.addTask(msg))
		{
			/* Переполнение очереди */
			reject(msg);
			tryTerminate();
			return;
		}
		
		if(newWorker)
		{
			if(worker.thread == null)
			{
				reject(msg);
				tryTerminate();
				return;
			}
			worker.thread.start();
		}
	}
	
	@Override
	public boolean awaitTermination(long timeout, TimeUnit unit) throws InterruptedException
	{
		long nanos = unit.toNanos(timeout);
		final ReentrantLock mainLock = this.mainLock;
		mainLock.lock();
		try
		{
			while(true)
			{
				if(state.get() == TERMINATED) return true;
				if(nanos <= 0) return false;
				nanos = termination.awaitNanos(nanos);
			}
		}
		finally
		{
			mainLock.unlock();
		}
	}

	@Override
	public void shutdown() throws SecurityException
	{
		final ReentrantLock mainLock = this.mainLock;
		mainLock.lock();
		try
		{
			checkShutdownAccess();
			advanceRunState(SHUTDOWN);
			interruptIdleWorkers(false);
		}
		finally
		{
			mainLock.unlock();
		}
		tryTerminate();
	}
	
	@Override
	public List<Runnable> shutdownNow() throws SecurityException
	{
		List<Runnable> tasks;
		final ReentrantLock mainLock = this.mainLock;
		mainLock.lock();
		try
		{
			checkShutdownAccess();
			advanceRunState(STOP);
			interruptWorkers();
			
			tasks = drainQueue();
		}
		finally
		{
			mainLock.unlock();
		}
		tryTerminate();
		return tasks;
	}
	
	@Override
	public boolean isShutdown()
	{
		return !isRunning(state.get());
	}
	
	@Override
	public boolean isTerminated()
	{
		return runStateAtLeast(state.get(), TERMINATED);
	}
	
	@Override
	protected void finalize()
	{
		SecurityManager sm = System.getSecurityManager();
		if(sm == null || acc == null) shutdown();
		else
		{
			PrivilegedAction<Void> pa = () -> { shutdown(); return null; };
			AccessController.doPrivileged(pa, acc);
		}
	}
	
	private void reject(Message msg)
	{
		System.err.println("Message rejected: "+msg);
	}

    /*
     * Methods for setting control state
     */

    /**
     * Transitions runState to given target, or leaves it alone if
     * already at least the given target.
     *
     * @param targetState the desired state, either SHUTDOWN or STOP
     *        (but not TIDYING or TERMINATED -- use tryTerminate for that)
     */
	private void advanceRunState(int targetState)
	{
		while(true)
		{
			int st = state.get();
			if(runStateAtLeast(st, targetState) || state.compareAndSet(st, targetState)) break;
		}
	}

    /**
     * Transitions to TERMINATED state if either (SHUTDOWN and pool
     * and queue empty) or (STOP and pool empty).  If otherwise
     * eligible to terminate but workerCount is nonzero, interrupts an
     * idle worker to ensure that shutdown signals propagate. This
     * method must be called following any action that might make
     * termination possible -- reducing worker count or removing tasks
     * from the queue during shutdown. The method is non-private to
     * allow access from ScheduledThreadPoolExecutor.
     */
     
    /*
    addWorkerFailed(), processWorkerExit(), shutdown(), shutdownNow(), remove()  
    */
	private final void tryTerminate()
	{
		while(true)
		{
			int st = state.get();
			System.out.printf("tryTerminate. state=%d\n", st);
			if(isRunning(st) || runStateAtLeast(st, TERMINATED)) return;
			final ReentrantLock mainLock = this.mainLock;
			if(st == SHUTDOWN)
			{
				mainLock.lock();
				try
				{
					//boolean allQueuesEmpty
					for(Worker worker: workers)
						if(worker != null && !worker.isQueueEmpty()) return;
				}
				finally
				{
					mainLock.unlock();
				}
			}
			
			// Eligible to terminate ???
			if(interruptIdleWorkers(true)) return;
			
			mainLock.lock();
			try
			{
				state.set(TERMINATED);
				termination.signalAll();
				return;
            }
            finally
            {
            	mainLock.unlock();
            }
            // else retry on failed CAS
        }
    }
	
    /*
     * Methods for controlling interrupts to worker threads.
     */
	
	/**
     * If there is a security manager, makes sure caller has
     * permission to shut down threads in general (see shutdownPerm).
     * If this passes, additionally makes sure the caller is allowed
     * to interrupt each worker thread. This might not be true even if
     * first check passed, if the SecurityManager treats some threads
     * specially.
     */
	private void checkShutdownAccess()
	{
		SecurityManager security = System.getSecurityManager();
		if(security == null) return;
		security.checkPermission(shutdownPerm);
		final ReentrantLock mainLock = this.mainLock;
		mainLock.lock();
		try
		{
			for(Worker worker: workers)
				if(worker != null) security.checkAccess(worker.thread);
		}
		finally
		{
			mainLock.unlock();
		}
	}

    /**
     * Interrupts all threads, even if active. Ignores SecurityExceptions
     * (in which case some threads may remain uninterrupted).
     */
	private void interruptWorkers()
	{
		final ReentrantLock mainLock = this.mainLock;
		mainLock.lock();
		try
		{
			for(Worker worker: workers)
				if(worker != null) worker.interruptIfStarted();
		}
		finally
		{
			mainLock.unlock();
		}
	}

    /**
     * Interrupts threads that might be waiting for tasks (as
     * indicated by not being locked) so they can check for
     * termination or configuration changes. Ignores
     * SecurityExceptions (in which case some threads may remain
     * uninterrupted).
     *
     * @param onlyOne If true, interrupt at most one worker. This is
     * called only from tryTerminate when termination is otherwise
     * enabled but there are still other workers.  In this case, at
     * most one waiting worker is interrupted to propagate shutdown
     * signals in case all threads are currently waiting.
     * Interrupting any arbitrary thread ensures that newly arriving
     * workers since shutdown began will also eventually exit.
     * To guarantee eventual termination, it suffices to always
     * interrupt only one idle worker, but shutdown() interrupts all
     * idle workers so that redundant workers exit promptly, not
     * waiting for a straggler task to finish.
     */
	private boolean interruptIdleWorkers(boolean onlyOne)
	{
		boolean interrupted = false;
		final ReentrantLock mainLock = this.mainLock;
		mainLock.lock();
		try
		{
			for(Worker worker: workers)
			{
				if(worker == null) continue;
				interrupted = true;
				Thread t = worker.thread;
				if(!t.isInterrupted() && worker.tryAcquire(1))
				{
					try
					{
						t.interrupt();
					}
					catch(SecurityException ignore) {}
					finally
					{
						worker.release(1);
					}
				}
				if(onlyOne) break;
			}
		}
		finally
		{
			mainLock.unlock();
		}
		return interrupted;
	}
	
	private List<Runnable> drainQueue()
	{
		List<Runnable> taskList = new ArrayList<>();
		final ReentrantLock mainLock = this.mainLock;
		mainLock.lock();
		try
		{
			for(Worker worker: workers) 
				if(worker != null) taskList.addAll(worker.drainQueue());
		}
		finally
		{
			mainLock.unlock();
		}
		return taskList;
	}
	
	
	private static boolean runStateLessThan(int c, int s)
	{
		return c < s;
	}
	
	private static boolean runStateAtLeast(int c, int s)
	{
		return c >= s;
	}
    
	private static boolean isRunning(int c)
	{
		return c < SHUTDOWN;
	}

	private final int poolSize;
	private final Worker[] workers;
	private final ReentrantLock mainLock = new ReentrantLock();
	private final Condition termination = mainLock.newCondition();
	private final AtomicInteger state = new AtomicInteger(RUNNING);

	private static final RuntimePermission shutdownPerm = new RuntimePermission("modifyThread");
	private final AccessControlContext acc;
	// runState is stored in the high-order bits
	
	static final ThreadFactory THREAD_FACTORY = Executors.defaultThreadFactory();
	
	private static final int RUNNING    = -1;
	private static final int SHUTDOWN   =  0;
	private static final int STOP       =  1;
	private static final int TERMINATED =  2;
	
	private class Worker extends AbstractQueuedSynchronizer implements Runnable
	{
		Worker(int targetId)
		{
			this.targetId = targetId;
			setState(-1);
			thread = THREAD_FACTORY.newThread(this);
		}
		
    /**
     * Main worker run loop.  Repeatedly gets tasks from queue and
     * executes them, while coping with a number of issues:
     *
     * 1. We may start out with an initial task, in which case we
     * don't need to get the first one. Otherwise, as long as pool is
     * running, we get tasks from getTask. If it returns null then the
     * worker exits due to changed pool state or configuration
     * parameters.  Other exits result from exception throws in
     * external code, in which case completedAbruptly holds, which
     * usually leads processWorkerExit to replace this thread.
     *
     * 2. Before running any task, the lock is acquired to prevent
     * other pool interrupts while the task is executing, and then we
     * ensure that unless pool is stopping, this thread does not have
     * its interrupt set.
     *
     * 3. Each task run is preceded by a call to beforeExecute, which
     * might throw an exception, in which case we cause thread to die
     * (breaking loop with completedAbruptly true) without processing
     * the task.
     *
     * 4. Assuming beforeExecute completes normally, we run the task,
     * gathering any of its thrown exceptions to send to afterExecute.
     * We separately handle RuntimeException, Error (both of which the
     * specs guarantee that we trap) and arbitrary Throwables.
     * Because we cannot rethrow Throwables within Runnable.run, we
     * wrap them within Errors on the way out (to the thread's
     * UncaughtExceptionHandler).  Any thrown exception also
     * conservatively causes thread to die.
     *
     * 5. After task.run completes, we call afterExecute, which may
     * also throw an exception, which will also cause thread to
     * die. According to JLS Sec 14.20, this exception is the one that
     * will be in effect even if task.run throws.
     *
     * The net effect of the exception mechanics is that afterExecute
     * and the thread's UncaughtExceptionHandler have as accurate
     * information as we can provide about any problems encountered by
     * user code.
     *
     * @param w the worker
     */
		@Override
		public void run()
		{
			System.out.println("Start worker: "+toString());
			Thread wt = Thread.currentThread();
			release(1); // allow interrupts
			boolean completedAbruptly = true;
			try
			{
				while(true)
				{
					Runnable task = getTask();
					if(task == null) break;
					acquire(1);
					// If pool is stopping, ensure thread is interrupted;
					// if not, ensure thread is not interrupted.  This
					// requires a recheck in second case to deal with
					// shutdownNow race while clearing interrupt
					if((runStateAtLeast(state.get(), STOP) || (Thread.interrupted() && runStateAtLeast(state.get(), STOP))) &&
						!wt.isInterrupted()) wt.interrupt();
					try
					{
						Throwable thrown = null;
						try
						{
							task.run();
						}
						catch(RuntimeException x)
						{
							thrown = x; throw x;
						}
						catch(Error x)
						{
							thrown = x; throw x;
						}
						catch(Throwable x)
						{
							thrown = x; throw new Error(x);
						}
					}
					finally
					{
						double elapsed = System.nanoTime() - startTime;
						System.out.printf("task processed: %s, %3.3f sec.\n", task, elapsed / 1000000000d);
						task = null;
						release(1);
					}
				}
				completedAbruptly = false;
			}
			finally
			{
				double elapsed = System.nanoTime() - startTime;
				System.out.printf("[%d] try terminate: %3.3f\n", targetId, elapsed / 1000000000d);
				tryTerminate();
			}
		}
		
    /**
     * Performs blocking or timed wait for a task, depending on
     * current configuration settings, or returns null if this worker
     * must exit because of any of:
     * 1. There are more than maximumPoolSize workers (due to
     *    a call to setMaximumPoolSize).
     * 2. The pool is stopped.
     * 3. The pool is shutdown and the queue is empty.
     * 4. This worker timed out waiting for a task, and timed-out
     *    workers are subject to termination (that is,
     *    {@code allowCoreThreadTimeOut || workerCount > corePoolSize})
     *    both before and after the timed wait, and if the queue is
     *    non-empty, this worker is not the last thread in the pool.
     *
     * @return task, or null if the worker must exit, in which case
     *         workerCount is decremented
     */
		private Runnable getTask()
		{
			while(true)
			{
				int st = state.get();
				// Check if queue empty only if necessary.
				if(st >= SHUTDOWN && (st >= STOP || queue.isEmpty())) return null;
				
				try
				{
					Runnable r = queue.take();
					if(r != null) return r;
				}
				catch(InterruptedException retry)
				{
					System.out.println("worker task interrupted. targetId: "+targetId);
				}
			}
		}
		// Lock methods
		//
		// The value 0 represents the unlocked state.
		// The value 1 represents the locked state.
		
		@Override
		protected boolean isHeldExclusively()
		{
			return getState() != 0;
		}
		
		@Override
		protected boolean tryAcquire(int unused)
		{
			if(compareAndSetState(0, 1))
			{
				setExclusiveOwnerThread(Thread.currentThread());
				return true;
			}
			return false;
		}
		
		@Override
		protected boolean tryRelease(int unused)
		{
			setExclusiveOwnerThread(null);
			setState(0);
			return true;
		}
		
		@Override
		public String toString()
		{
			return "["+targetId+"] "+queue;
		}
		
		boolean addTask(Message msg)
		{
			return queue.offer(msg);
		}
		
		boolean isQueueEmpty()
		{
			return queue.isEmpty();
		}
		
		List<Runnable> drainQueue()
		{
			BlockingQueue<Runnable> q = queue;
			List<Runnable> taskList = new ArrayList<>();
			q.drainTo(taskList);
			if(!q.isEmpty())
			{
				for(Runnable r: q.toArray(new Runnable[0]))
					if(q.remove(r)) taskList.add(r);
			}
			return taskList;
		}
		
		void interruptIfStarted()
		{
			Thread t;
			if(getState() >= 0 && (t = thread) != null && !t.isInterrupted())
			{
				try
				{
					t.interrupt();
				}
				catch(SecurityException ignore) {}
			}
		}
		
		final Thread thread;
		
		private final int targetId;
		private final long startTime = System.nanoTime();
		private final BlockingQueue<Runnable> queue = new LinkedBlockingQueue<>();
		
		/* run serialver */
		private static final long serialVersionUID = -20668143327078549L;
    }
}
