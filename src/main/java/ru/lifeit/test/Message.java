package ru.lifeit.test;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.StringReader;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import java.util.stream.Collectors;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;

import javax.xml.transform.dom.DOMSource;

import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.DOMException;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

class Message implements Runnable
{
    public static synchronized Message generate(int id, int carsCount) throws SAXException, IOException, ParserConfigurationException
    {
        int randomCarId = RND.nextInt(carsCount)+1;
		DocumentBuilder documentBuilder = DBF.newDocumentBuilder();
		StringBuilder sb = new StringBuilder()
			.append("<?xml version=\"1.0\" encoding=\"utf-8\"?>\n")
			.append("<message>\n")
			.append("	<target id=\"")
			.append(randomCarId)
			.append("\" />\n")
			.append("	<sometags>\n")
			.append("		<data>a</data>\n")
			.append("		<data>b</data>\n")
			.append("		<data>c</data>\n")
			.append("	</sometags>\n")
			.append("</message>\n");
		Document doc = documentBuilder.parse(new InputSource(new StringReader(sb.toString())));
		return new Message(id, doc);
    }
    
	private Message(int id, Document doc) throws DOMException
	{
	    this.id = id;
		NodeList nl = doc.getElementsByTagName("target");
		if(nl.getLength() == 0) throw new DOMException(DOMException.NOT_FOUND_ERR, "Tag <target> not found!");
		Element target = (Element)nl.item(0);
		try
		{
			targetId = Integer.parseInt(target.getAttribute("id"));
		}
		catch(NumberFormatException e)
		{
			throw new DOMException(DOMException.NOT_FOUND_ERR, "Attribute `id` in tag <target> not found!");
		}
		nl = doc.getElementsByTagName("sometags");
		if(nl.getLength() != 0)
		{
			Element someTags = (Element)nl.item(0);
			nl = someTags.getElementsByTagName("data");
			for(int i = 0; i < nl.getLength(); ++i)
				dataList.add(new Data(((Element)nl.item(i)).getTextContent()));
		}
	}
	
	@Override
	public void run()
	{
	    try
	    {
	        Document doc = DBF.newDocumentBuilder().newDocument();
	        // Create DOM
	        Element msg = doc.createElement("message");
	        doc.appendChild(msg);
	        Element target = doc.createElement("target");
	        target.setAttribute("id", ""+targetId);
	        msg.appendChild(target);
	        Element dispatched = doc.createElement("dispatched");
	        dispatched.setAttribute("id", ""+id);
	        msg.appendChild(dispatched);
	        if(dataList.size() > 0)
	        {
	            Element someTags = doc.createElement("sometags");
	            msg.appendChild(someTags);
	            for(Data data: dataList) data.append(doc, someTags);
	        }
		
	        Transformer transformer = TransformerFactory.newInstance().newTransformer();
	        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
		
	        File file = new File("data/"+targetId+"/"+id+".xml");
	        System.out.println("saving file: "+file.getAbsolutePath());
	        transformer.transform(new DOMSource(doc), new StreamResult(file));
		}
		catch(Exception e)
		{
			System.err.println("Error saving message! Id: "+id+". "+e.getMessage());
			return;
		}
		
		try
		{
			Thread.sleep(DELAY);
		}
		catch(InterruptedException e)
		{
		    System.out.println("Executor interrupted. "+targetId);
		}
	}
	
	public int getId()
	{
	    return id;
	}
	
	public int getTargetId()
	{
		return targetId;
	}
	
	public List<Data> getDataList()
	{
		return dataList;
	}
	
	public void save(Document doc) throws TransformerException
	{
	}
	
	public long getTimestamp()
	{
	    return timestamp;
	}
	
	@Override
	public String toString()
	{
		StringBuilder sb = new StringBuilder("[")
		    .append(id)
			.append(", targetId: ")
			.append(targetId)
			.append(", data: ")
			.append(dataList.stream().map(d -> d.getValue()).collect(Collectors.joining(", ", "{", "}")))
			.append("]");
		return sb.toString();
	}
	
	@Override
	public int hashCode()
	{
	    return id;
	}
	
	@Override
	public boolean equals(Object obj)
	{
	    if(obj == null || !(obj instanceof Message)) return false;
	    Message other = (Message)obj;
	    return other.id == id;
	}
	
	private int id;
	private int targetId;
	private long timestamp = System.currentTimeMillis();
	private List<Data> dataList = new ArrayList<>();
	
	private static final int DELAY = 3*1000;
	private static final Random RND = new Random();
	private static final DocumentBuilderFactory DBF = DocumentBuilderFactory.newInstance();
}
